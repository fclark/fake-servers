/*
 * grunt
 * http://gruntjs.com/
 *
 * Copyright (c) 2013 "Cowboy" Ben Alman
 * Licensed under the MIT license.
 * https://github.com/gruntjs/grunt/blob/master/LICENSE-MIT
 */

'use strict';

var path = require('path');
module.exports = function (grunt) {
    // Project configuration.
    grunt.initConfig({
        watch: {
            scripts: {
                files: 'siteapi.js',
                tasks: ['default'],
                options: {
                    interrupt: true,
                },
            },
        },
        express: {
            fake_cms: {
                options: {
                    hostname: 'localhost',
                    port: 8002,
                    server: path.resolve(__dirname, 'cms.js')
                }
            },
            fake_siteapi: {
                options: {
                    hostname: 'localhost',
                    port: 7002,
                    server: path.resolve(__dirname, 'siteapi.js')
                }
            }
        },

    });

    // Load the plugin that provides the "requirejs" task.
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-express');

    // "npm test" runs these tasks
    grunt.registerTask('default', [
        'express',
        'watch'
    ]);

};


