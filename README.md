fake-servers
============

# Getting started

Install and run servers

```
$ git clone https://fclark@bitbucket.org/fclark/fake-servers.git
$ cd fake-servers
$ npm install
$ grunt
```

# Usage

While your servers are running you can use a tool like curl or Postman REST Client to consume the endpoints 

Example:
```
$ curl -XGET http://localhost:7002/siteapi/register
{"first_name":"","last_name":"","middle_initial":"","suffix":"","home_address":"","city":"","state":"","state_of_residence":"","zip_code":"","date_of_birth":"1/1/0001","email":"","password":"","credit_report_authorization":false,"privacy_policies":false,"prosper_acceptance_code":"","loan_purpose":0,"credit_grade":0,"ssn":"","referral_code":"","is_read_only":false}
```

This starts the cms and the fake "siteapi" server.  The "siteapi" server serves all endpoints for many prosper applications.