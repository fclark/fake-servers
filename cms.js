var request = require('request');
var express = require('express');
var app = express();

//CORS middleware
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
}

app.use(allowCrossDomain);

app.get('/cblock/*', function(req, res, path){
    'use strict';
    var contentPath = req.url;
    var target = 'http://content.cms.c1.dev' + contentPath;
    request(target, function (error, response, body) {
        if (!error) {
            if (response.statusCode === 200) {
                res.send(body);
            } else if (response.statusCode === 404) {
                res.status(404);
                res.type('txt').send('404 Not Found');
            }
        } else {
            res.status(404);
            res.type('txt').send('404 Not Found');
        }
    });
});
app.get('/lp/*', function(req, res, path){
    'use strict';
    var contentPath = req.url;
    var target = 'http://content.cms.c1.dev' + contentPath;
    request(target, function (error, response, body) {
        if (!error) {
            if (response.statusCode === 200) {
                res.send(body);
            } else if (response.statusCode === 404) {
                res.status(404);
                res.type('txt').send('404 Not Found');
            }
        } else {
            res.status(404);
            res.type('txt').send('404 Not Found');
        }
    });
});
module.exports = app;
