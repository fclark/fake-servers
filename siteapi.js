'use strict';

var fs = require('fs');
var express = require('express');
var path = require('path');
var app = express();

//CORS middleware
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
};

var supportedJSONRoutes = [
    '/siteapi/offer',
    '/siteapi/campaign',
    '/siteapi/documents/userworkflows',
    '/siteapi/email',
    '/siteapi/impactradius',
    '/siteapi/register',
    '/siteapi/appbyphoneregister',
    '/siteapi/abtest',
    '/siteapi/bank',
    '/siteapi/personaldetails',
    '/siteapi/tiladetails',
    '/siteapi/selectedoffer',
    '/siteapi/prospecttoborrower',
    '/siteapi/referral/createnewprospect',
    '/siteapi/referral/validateoffercode',
    '/siteapi/referral/validatepartnermembership',
    '/siteapi/referral/verifyoffercode',
    '/siteapi/referral/verifypartnermembership',
    '/siteapi/referral/verifyprospect',
    '/siteapi/referral/createnewprospect',
    '/siteapi/password',
    '/siteapi/password/verify',
    '/siteapi/abandon',
    '/siteapi/loan',
    '/siteapi/futurepayment/getfutureamountsdue',
    '/siteapi/futurepayment/postonetimepayment',
    '/siteapi/borrowerbankaccount',
    '/siteapi/makepayment/dismissmakepaymentpopup'
];
var supportedHTMLRoutes = [
    '/siteapi/agreements/tila',
    '/siteapi/agreements/1500',
    '/siteapi/agreements/1501',
    '/siteapi/agreements/1502'
];
var jsonResponse = function (route, reqMethod) {
    var response;
    var method = reqMethod || 'post';
    var format = 'json';
    var configPath = path.resolve(__dirname, './siteapi' + route + '/response-' + method + '.' + format);
    try {
        // attempt to require custom response
        response = require(configPath);
    } catch (e) {
        // set default success response
        response = {
            status: {
                errors: [],
                validation_errors: []
            }
        };
    }
    return response;
};

var sendHTMLResponse = function (route, reqMethod, callback) {
    var response;
    var method = reqMethod || 'get';
    var format = 'html';
    var configPath = path.resolve(__dirname, './siteapi' + route + '/response-' + method + '.' + format);
    fs.readFile(configPath, 'utf8', function (err, data) {
        if (err) {
            response = '<h2>SUCCESS: No Template Provided</h2>'
        } else {
            response = data
        }
        callback(response)
    });
};

app.use(allowCrossDomain);

for (var i = 0; i < supportedJSONRoutes.length; i += 1) {
    // use closure to preserve route
    (function (route) {
        app.get(route, function (req, res) {
            setTimeout(function () {
                res.json(jsonResponse(route, 'get'));
            }, 2000);
        });
        app.post(route, function (req, res) {
            setTimeout(function () {
                res.json(jsonResponse(route, 'post'));
            }, 2000);
        });
        app.put(route, function (req, res) {
            setTimeout(function () {
                res.json(jsonResponse(route, 'put'));
            }, 2000);
        });
    })(supportedJSONRoutes[i]);
}
for (var i = 0; i < supportedHTMLRoutes.length; i += 1) {
    // use closure to preserve route
    (function (route) {
        app.get(route, function (req, res) {
            setTimeout(function () {
                sendHTMLResponse(route, 'get', function (data) {
                    res.send(data);
                });
            }, 2000);
        });
        app.post(route, function (req, res) {
            setTimeout(function () {
                sendHTMLResponse(route, 'post', function (data) {
                    res.send(data);
                });
            }, 2000);
        });
        app.put(route, function (req, res) {
            setTimeout(function () {
                sendHTMLResponse(route, 'put', function (data) {
                    res.send(data);
                });
            }, 2000);
        });
    })(supportedHTMLRoutes[i]);
}

module.exports = app;
